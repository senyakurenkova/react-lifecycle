import logo from './logo.svg';
import './App.css';
import React from 'react';

const arr =[1,1,2,3,5,8,13,21];
function App(props) {
  const [date, setDate] = React.useState(new Date());

  React.useEffect(() =>{
    console.log('Монтирование - первоначальный рендеринг компонента в DOM');
    let timer = setInterval(() => setDate(new Date()), 1000);

    return() =>{
      console.log('Размонтирование для сохранения ресурсов');
      clearInterval(timer);
    }
  }, [date]);

  return(
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" /> 
        <p>
          Hello, edit <code>src/App.js</code> and save to reload.
        </p>
        <p>
          Now {date.toLocaleTimeString}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>bool: {true}</p>
        <p>num: {12}</p>
        <p>null: {null}</p>
        <p>string: {'str'}</p>
        <ul>{arr.map((elem) => <li>{elem}</li>)}</ul>
      </header>
    </div>
  );
};

// class App extends React.Component{
//   constructor(props){
//     super(props);
//     this.state = {date: new Date()}
//   }
//   componentDidMount(){
//     console.log('Монтирование - первоначальный рендеринг компонента в DOM');
//     this.timer = setInterval(() => this.setState(new Date()), 1000);
//   }
//   componentWillUnmount(){
//     console.log('Размонтирование для сохранения ресурсов');
//     clearInterval(this.timer);
//   }
//   render(){
//     return(
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" /> 
//         <p>
//           Hello, edit <code>src/App.js</code> and save to reload.
//         </p>
//         <p>
//           Now {this.state.date.toLocaleTimeString}
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//         <p>bool: {true}</p>
//         <p>num: {12}</p>
//         <p>null: {null}</p>
//         <p>string: {'str'}</p>
//         <ul>{arr.map((elem) => <li>{elem}</li>)}</ul>
//       </header>
//     </div>
//   );
//   }
// }

 export default App;
